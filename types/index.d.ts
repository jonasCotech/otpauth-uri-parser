export interface OtpauthUriParser {
  /**
   * This function takes an otpauth:// style key URI and parses it into an object with keys for the
   * various parts of the URI
   *
   * @param {String} uri The otpauth:// uri that you want to parse
   *
   * @return {Object} The parsed URI or null on failure. The URI object looks like this:
   *
   * {
   *  type: 'totp',
   *  label: { issuer: 'ACME Co', account: 'jane@example.com' },
   *  query: {
   *   secret: 'JBSWY3DPEHPK3PXP',
   *   digits: '6'
   *  }
   * }
   *
   * @see <a href="https://github.com/google/google-authenticator/wiki/Key-Uri-Format">otpauth Key URI Format</a>
   */
  (uri: string): any;
}

declare const otpauthUriParser: OtpauthUriParser;
export default otpauthUriParser;